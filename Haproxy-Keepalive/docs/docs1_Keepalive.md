
### Mục lục

- [I. Keepalive](#Keepalive)
	* [1. Keepalive là gì](#Keepalive_1)


<a name="Keepalive"></a>
### **I. Keepalive**

<a name="Keepalive_1"></a>
#### **1. Keepalive là gì**
Keepalive là những gói tin (packet) chứa thông điệp được gửi từ một thiết bị (router, switch, vps, cloud server,..) đến một thiết bị khác trong mô hình hệ thống mạng HA với mục đích là kiểm tra trạng thái đường kết nối giữa hai thiết bị có hoạt động không hay thiết bị đầu cuối còn sống không. Gói tin thông điệp Keepalive thường rất nhỏ và chiếm rất ít về băng thông, còn nội dung của gói tin keepalive phụ thuộc vào thiết kế của các giao thức hoặc dịch vụ. 
- Keepalive có các tham số :  

`keepalive interval`: thời gian giữa các lần gửi gói tin keepalive từ thiết bị. Giá trị này thường do người dùng cấu hình thủ công.   
`keepalive retries`: số lần mà một thiết bị cố gắng gửi gói tin keepalive kiểm tra trạng thái khi không nhận được phản hồi từ thiết bị khác. Nếu quá số lần này thì có thể xem là thiết bị đầu kia có trạng thái đường kết nối đứt hoặc đã chết. 
- Nguyên lý hoạt động:
   * Một gói tin keepalive sẽ được gửi từ một thiết bị A với số thời gian quy định giữa các lần gửi, đến thiết bị B.
   * Sau khi gói tin keepalive đó gửi đi, A sẽ mong chờ một gói tin phản hồi từ B để kiểm tra đường kết nối giữa hai thiết bị đang hoạt động ổn định.
   * Nếu không nhận được gói tin phản hồi, thiết bị A sẽ gửi tiếp một số lần thử gửi lại gói tin (retries) và chờ đợi tiếp.
  * Nếu sau ’n’ lần không gửi gói tin keepalive vẫn không nhận được phản hồi thì lúc này, thiết bị A sẽ xem như thiết bị B đã chết hoặc đường truyền giữa 2 thiết bị đã ‘down’.
  * Lúc này dịch vụ sẽ quyết định các hoạt động khác nếu thiết bị B chết như là chuyển hướng data sang thiết bị khác,…  



REF: [Wiki](https://en.wikipedia.org/wiki/Keepalive)